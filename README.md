# Fixit Students! #
[![pipeline status](https://git.informatik.uni-leipzig.de/as37zehi/fixit_students/badges/master/pipeline.svg)](https://git.informatik.uni-leipzig.de/as37zehi/fixit_students/commits/master)
[![License](https://img.shields.io/badge/License-MPL%202.0-green.svg)](https://github.com/rmeissn/fixit_students/blob/master/LICENSE)
[![Language](https://img.shields.io/badge/Language-Javascript%20ECMA2015-lightgrey.svg)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
[![Framework](https://img.shields.io/badge/Framework-NodeJS%208.9-blue.svg)](https://nodejs.org/)
[![Webserver](https://img.shields.io/badge/Webserver-Hapi%2016-blue.svg)](http://hapijs.com/)
[![LinesOfCode](https://img.shields.io/badge/LOC-398-lightgrey.svg)](https://github.com/rmeissn/fixit_students/blob/master/package.json#L16)

This repository contains code of a NodeJS based microservice. The developer of the project is a bit lazy and students of the Leipzig University, course SWT-17 are inteded to fix all issues.

<!--**Remember to exchange badge urls when forking!**-->

### Prerequisites ###

* Install [NodeJS v8](https://nodejs.org/en/) LTS on your system
* Fork the Repository to your own user account at Gitlab
* Clone the forked repository to your pc
* Open a CLI (**C**ommand-**L**ine **I**nterface) and change into the folder
* Run `npm install`

### Where to start? ###

As we're developing our application with NodeJS, we're using [npm](https://docs.npmjs.com/) as a **task runner**. Have a look at the file [package.json's](https://git.informatik.uni-leipzig.de/meissner/fixit_students/blob/master/package.json) script section to obtain an overview of available commands.

**Head over to the file `TODO.md` and do the listed tasks.**

Some of these are:

```
# Run syntax check and lint your code
npm run lint

# Run unit tests
npm run unit:test

# Start the application
npm start
...
```

Run the following commands on your CLI and have a look at the results:

```
npm run lint
npm run test:integration
npm run coverage
npm run countLOC
```

If you want to dig through the code, have a look at the file [server.js](https://git.informatik.uni-leipzig.de/meissner/fixit_students/blob/master/server.js), that is the main routine of this service. Follow the **require(...)** statements to dig trough the entire code in the correct order.

If you want to have a look at **tests**, head over to the folder [tests/](https://git.informatik.uni-leipzig.de/meissner/fixit_students/blob/master//tests). We're using the frameworks Mocha and Chai for our purposes.

You want to **checkout how this service works in production**? Simply start the service by running `npm start` and head over to: [http://localhost:3000/documentation](http://localhost:3000/documentation). We're using [Swagger](https://www.npmjs.com/package/hapi-swagger) to generate this API discrovery/documentation page. BTW.: Did you already discoverd the super easy swagger integration inside [routes.js](https://git.informatik.uni-leipzig.de/meissner/fixit_students/blob/master/routes.js)? Tags 'api' and 'description' were everything we needed to add.

### What's about Continuous Integration/Delivery? ###
---
Continuous Integration (CI) and possibly Continuous Delivery (CD) is currently setup by using Gitlab CI. Have a look at the [Gitlab CI Documentation](https://docs.gitlab.com/ce/ci/) to gain an overview about CI and CD and to learn about possibilities.

A widely known alternative to Gitlab CI is the (for OSS projects) free to use web application [Travis-CI](https://travis-ci.org/). This is the default CI for Github and many more resource hosting services.
